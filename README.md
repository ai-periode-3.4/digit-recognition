# Digit Recognition

Digit recognition with Tensorflow ML in Python.

## <br />Prequisites

<ul>
    <li>Python 3.8</li>
    <li>Tensorflow (via pip install or anaconda)</li>
    <li>Keras (via pip install or anaconda)</li>
    <li>MNIST dataset</li>
</ul>

## <br />Data
Used dataset: [MNIST](http://yann.lecun.com/exdb/mnist/).
<br /> This dataset contains 60.000 training digits/images (28 x 28 pixels) and labels ranging from 0 to 9.
<br /> This dataset also contains 10.000 test digits/images (28 x 28 pixels) and labels ranging from 0 to 9 in order to test the trained model.


## <br />Binary classification
<code>digit_classification_bin.py</code> focusses on recognizing digits that are 'not one' (0) or 'one' (1). After performing the ML learning, it is visible that the learning model for this task does not have to be complex. With just simply one (hidden) layer containing 1 perceptron and the activation 'relu', it achieves an accuracy of atleast 0.99 with a low count of false predictions (55) (epochs = 10).

## <br />Digit classification
<code>digit_classification.py</code> focusses on recognizing digits ranging from 0 to 9. After performing the ML learning, it is visible that the learning model for this task does not have to be complex. With just simply one (hidden) layer containing 16 perceptron and the activation 'relu', it achieves an accuracy of atleast 0.99 with a very low count of false predictions (55) (epochs = 10).
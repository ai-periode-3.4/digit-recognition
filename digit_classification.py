import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt

# Load nmist data
digit_mnist = tf.keras.datasets.mnist
(train_data, train_labels), (test_data, test_labels) = digit_mnist.load_data()

# Normalize pixel greyscale value from 0-255 to 0-1 float value,
# so that the accuracy of the model increases
train_data, test_data = train_data / 255.0, test_data / 255.0

print('Train digits shape: ', train_data.shape)
print('Train digits: ', len(train_data))
print('Train labels: ', len(train_labels))

print('\nTest digits shape: ', test_data.shape)
print('Test digits: ', len(test_data))
print('Test labels: ', len(test_labels))

class_names = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten']

# First layer 'Flatten' transforms the format of the image from a two-dimensional array (28 x 28 px) to a one-dimensional array (of 28 * 28 = 784 pixels)
# This is to unstack rows of pixels in image and lining them up.

# Second layer 'Dense' contains <number> neurons. (hidden layers)
# Third layer 'Dense' returns a logits array with length of 10. Each neuron contains a score that indicates the current image belongs to one of the 10 classes.
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(16, activation=tf.nn.tanh), # Neurons
    tf.keras.layers.Dense(10, activation=tf.nn.softmax) # Output
])

# Compile model
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

# Train model
model.fit(train_data, train_labels, epochs=10)

# Model loss, model accuracy
test_loss, test_acc = model.evaluate(test_data,  test_labels, verbose=2)

print('\nTest accuracy:', test_acc, '\n')

# Create prediction
probability_model = tf.keras.Sequential([model, tf.keras.layers.Softmax()])

predictions = probability_model.predict(test_data)

# Plot AUR ROC
predictions_y_max = []
predictions_count = len(predictions)
for i in range(0, predictions_count):
  predictions_y_max.append(np.argmax(i))

from sklearn.metrics import roc_curve,roc_auc_score
fpr , tpr , thresholds = roc_curve (test_labels, predictions_y_max)
def plot_roc_curve(fpr,tpr): 
  plt.plot(fpr,tpr) 
  plt.axis([0,1,0,1]) 
  plt.xlabel('False Positive Rate') 
  plt.ylabel('True Positive Rate') 
  plt.show()    
plot_roc_curve (fpr,tpr) 

# Count false predictions
false_prediction_count = 0
for i in range(0, predictions_count):
  if (np.argmax(predictions[i]) != test_labels[i]):
    false_prediction_count += 1

print('\nNumber of false predictions: ', false_prediction_count)

#
# Plot image
#
def plot_image(i, predictions_array, true_label, img):
  true_label, img = true_label[i], img[i]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])

  plt.imshow(img, cmap=plt.cm.binary)

  predicted_label = np.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'green'
  else:
    color = 'red'

  plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                100*np.max(predictions_array),
                                class_names[true_label]),
                                color=color)

#
# Value array
#
def plot_value_array(i, predictions_array, true_label):
  true_label = true_label[i]
  plt.grid(False)
  plt.xticks(range(10))
  plt.yticks([])
  thisplot = plt.bar(range(10), predictions_array, color="#777777")
  plt.ylim([0, 1])
  predicted_label = np.argmax(predictions_array)

  thisplot[predicted_label].set_color('red')
  thisplot[true_label].set_color('blue')

# Plot the first X test images, their predicted labels, and the true labels.
# Color correct predictions in blue and incorrect predictions in red.
num_rows = 5
num_cols = 3
num_images = num_rows*num_cols
plt.figure(figsize=(2*2*num_cols, 2*num_rows))
for i in range(num_images):
  plt.subplot(num_rows, 2*num_cols, 2*i+1)
  plot_image(i, predictions[i], test_labels, test_data)
  plt.subplot(num_rows, 2*num_cols, 2*i+2)
  plot_value_array(i, predictions[i], test_labels)
plt.tight_layout()
plt.show()
